<?php

namespace Bss\WhitelistIP\Plugin;

/**
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 */
class Authentication
{
    /**
     * @var \Bss\WhitelistIP\Model\AutoAuth
     */
    protected $autoAuth;

    /**
     * Authentication constructor.
     * @param \Bss\WhitelistIP\Model\AutoAuth $autoAuth
     */
    public function __construct(
        \Bss\WhitelistIP\Model\AutoAuth $autoAuth
    ) {
        $this->autoAuth = $autoAuth;
    }

    /**
     * @param \Magento\Backend\App\AbstractAction $subject
     * @param \Magento\Framework\App\RequestInterface $request
     * @return \Magento\Framework\App\RequestInterface[]
     * @throws \Magento\Framework\Exception\InputException
     * @throws \Magento\Framework\Stdlib\Cookie\CookieSizeLimitReachedException
     * @throws \Magento\Framework\Stdlib\Cookie\FailureToSendException
     */
    public function beforeDispatch(
        \Magento\Backend\App\AbstractAction $subject,
        \Magento\Framework\App\RequestInterface $request
    ) {
        $requestedActionName = $request->getActionName();
        if ($requestedActionName == 'autologin' && $this->autoAuth->login()) {
            // fake action name to pass prolong() in Magento\Backend\App\Action\Plugin\Authentication
            $request->setActionName('refresh');
        }
        return [$request];
    }
}
