<?php

namespace Bss\WhitelistIP\Controller\Adminhtml\AutoLogin;

use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Framework\Controller\Result\RedirectFactory;

class AutoLogin extends Action
{
    /**
     * @var RedirectFactory
     */
    protected $redirectResultFactory;

    /**
     * @param Context $context
     * @param RedirectFactory $redirectResultFactory
     */
    public function __construct(
        Context $context,
        RedirectFactory $redirectResultFactory
    ) {
        $this->redirectResultFactory = $redirectResultFactory;
        parent::__construct($context);
    }

    /**
     *
     * @return \Magento\Framework\Controller\Result\Redirect
     */
    public function execute()
    {
        $result = $this->redirectResultFactory->create();
        $result->setPath('admin/dashboard/index');
        return $result;
    }
}
