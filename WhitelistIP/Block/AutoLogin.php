<?php
namespace Bss\WhitelistIP\Block;

use Bss\WhitelistIP\Model\AutoAuth;
use Magento\Framework\View\Element\Template;

class AutoLogin extends Template
{
    /**
     * @var AutoAuth
     */
    protected $autoAuth;

    /**
     * AutoLogin constructor.
     * @param AutoAuth $autoAutho
     * @param Template\Context $context
     * @param array $data
     */
    public function __construct(
        AutoAuth $autoAuth,
        Template\Context $context,
        array $data = []
    ) {
        $this->autoAuth = $autoAuth;
        parent::__construct($context, $data);
    }

    /**
     * @return bool
     */
    public function checkEnable()
    {
        return $this->autoAuth->isEnable() && $this->autoAuth->validateIp();
    }
}
