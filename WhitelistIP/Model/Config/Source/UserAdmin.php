<?php

namespace Bss\WhitelistIP\Model\Config\Source;

use Magento\User\Model\ResourceModel\User\CollectionFactory;

class UserAdmin implements \Magento\Framework\Data\OptionSourceInterface
{
    /**
     * @var CollectionFactory
     */protected $_userCollection;

    public function __construct(
        CollectionFactory $userCollection
    ) {
        $this->_userCollection = $userCollection;
    }

    /**
     * @return array
     */
    public function toOptionArray()
    {
        $arr = [];
        $users = $this->_userCollection->create();
        foreach ($users as $user){
            $arr[] = ['value' => $user->getId(), 'label' => $user->getName() . ' - ' . $user->getEmail()];
        }
        return $arr;
    }
}
