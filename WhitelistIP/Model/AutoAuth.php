<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Bss\WhitelistIP\Model;

use Magento\Backend\Model\Auth\Session;
use Magento\Backend\Model\Session\AdminConfig;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\HTTP\PhpEnvironment\RemoteAddress;
use Magento\Framework\Stdlib\Cookie\CookieMetadataFactory;
use Magento\Framework\Stdlib\CookieManagerInterface;
use Magento\Security\Model\AdminSessionsManager;
use Magento\User\Model\UserFactory;

class AutoAuth
{
    /**
     * @var UserFactory
     */
    protected $userFactory;

    /**
     * @var Session
     */
    protected $session;

    /**
     * @var CookieManagerInterface
     */
    protected $cookie;

    /**
     * @var AdminConfig
     */
    protected $adminConfig;

    /**
     * @var CookieMetadataFactory
     */
    protected $cookieMetadata;

    /**
     * @var AdminSessionsManager
     */
    protected $adminSession;

    /**
     * @var ScopeConfigInterface
     */
    protected $scopeConfig;

    /**
     * @var RemoteAddress
     */
    protected $remoteAddress;

    /**
     * AutoAuth constructor.
     * @param UserFactory $userFactory
     * @param Session $session
     * @param CookieManagerInterface $cookie
     * @param AdminConfig $adminConfig
     * @param CookieMetadataFactory $cookieMetadata
     * @param AdminSessionsManager $adminSession
     * @param ScopeConfigInterface $scopeConfig
     * @param RemoteAddress $remoteAddress
     */
    public function __construct(
        UserFactory $userFactory,
        Session $session,
        CookieManagerInterface $cookie,
        AdminConfig $adminConfig,
        CookieMetadataFactory $cookieMetadata,
        AdminSessionsManager $adminSession,
        ScopeConfigInterface $scopeConfig,
        RemoteAddress $remoteAddress
    ) {
        $this->userFactory = $userFactory;
        $this->session = $session;
        $this->cookie = $cookie;
        $this->adminConfig = $adminConfig;
        $this->cookieMetadata = $cookieMetadata;
        $this->adminSession = $adminSession;
        $this->scopeConfig = $scopeConfig;
        $this->remoteAddress = $remoteAddress;
    }

    /**
     * @return bool
     * @throws \Magento\Framework\Exception\InputException
     * @throws \Magento\Framework\Stdlib\Cookie\CookieSizeLimitReachedException
     * @throws \Magento\Framework\Stdlib\Cookie\FailureToSendException
     * @return bool
     */
    public function login()
    {
        if ($this->isEnable() && $this->validateIp()) {
            $user = $this->getUser();
            if (!isset($user)) {
                return false;
            }
            $this->session->setUser($user);
            $this->session->processLogin();
            if ($this->session->isLoggedIn()) {
                $cookieValue = $this->session->getSessionId();
                if ($cookieValue) {
                    $this->adminSession->processLogin();
                    $timelife = $this->adminSession::ADMIN_SESSION_LIFETIME;
                    $publicCookieMetadata = $this->cookieMetadata->createPublicCookieMetadata();
                    $publicCookieMetadata->setDuration($timelife)
                        ->setPath($this->adminConfig->getCookiePath())
                        ->setDomain($this->adminConfig->getCookieDomain())
                        ->setSecure($this->adminConfig->getCookieSecure())
                        ->setHttpOnly(($this->adminConfig->getCookieHttpOnly()));
                    $this->cookie->setPublicCookie($this->session->getName(), $cookieValue, $publicCookieMetadata);
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * @return bool
     */
    public function isEnable()
    {
        return $this->scopeConfig->getValue(
            'whitelist_ip/whitelist_config/whitelist_enable',
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
    }

    /**
     * @return \Magento\User\Model\User
     */
    protected function getUser()
    {
        $userId = $this->scopeConfig->getValue(
            'whitelist_ip/whitelist_config/whitelist_admin',
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
        $user = $this->userFactory->create();
        return $user->load($userId);
    }

    /**
     * @return bool
     */
    public function validateIp()
    {
        $configIps = $this->scopeConfig->getValue(
            'whitelist_ip/whitelist_config/whitelist_ip_address',
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
        $configIps = preg_replace('/\s+/', '', $configIps);
        $listIps = explode(",", $configIps);
        foreach ($listIps as $configIp) {
            if ($configIp == $this->remoteAddress->getRemoteAddress()) {
                return true;
            }
        }
        return false;
    }
}
